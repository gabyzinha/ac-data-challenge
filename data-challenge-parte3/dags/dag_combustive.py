from airflow import DAG
from airflow.utils.dates import datetime
from airflow.operators.dummy import DummyOperator
from airflow.operators.python import PythonOperator
from airflow.providers.http.operators.http import SimpleHttpOperator
from airflow.providers.google.cloud.operators.dataproc import (
    DataprocCreateClusterOperator,
    DataprocDeleteClusterOperator,
    DataprocSubmitJobOperator,
)
from google.cloud import storage
import logging

GCP_CONNECTION='google_cloud_default'
CLUSTER_NAME = 'dc-cluster'
REGION='us-central1' # region
PROJECT_ID='ac-data-challenge' #project name
PYSPARK_URI='gs://dc-solution-bucket/main-etl.py' # spark job location in cloud storage
URL = 'https://www.gov.br/anp/pt-br/centrais-de-conteudo/dados-abertos/arquivos/shpc/dsas/ca/ca-2021-02.csv'
FILE_NAME = URL.split('/')[-1]
BUCKET_NAME = 'dc-solution-bucket'
GCS_FILE_PATH = 'gs://dc-solution-bucket/airfow/'
LOCAL_PATH = f'/tmp/{FILE_NAME}'
JOB_FILE_URI = 'gs://dc-solution-bucket/main-etl.py'

CLUSTER_CONFIG = {
        "master_config": {
            "num_instances": 1,
            "machine_type_uri": "n2-standard-2",
            "disk_config": {"boot_disk_type": "pd-standard", "boot_disk_size_gb": 500},
        },
        "worker_config": {
            "num_instances": 2,
            "machine_type_uri": "n2-standard-2",
            "disk_config": {"boot_disk_type": "pd-standard", "boot_disk_size_gb": 500},
        },
}

PYSPARK_JOB = {
    "reference": {"project_id": PROJECT_ID},
    "placement": {"cluster_name": CLUSTER_NAME},
    "pyspark_job": {
        "main_python_file_uri": JOB_FILE_URI,
        "jar_file_uris": ['https://repo1.maven.org/maven2/com/google/cloud/spark/spark-bigquery-with-dependencies_2.12/0.36.2/spark-bigquery-with-dependencies_2.12-0.36.2.jar'],
        "args": [
            f"gs://dc-solution-bucket/data/{FILE_NAME}",
            f"gs://dc-solution-bucket/output-data/",
            "PARQUET",
            "dc_etl_airflow.combustivel_automotivo"
        ],
    },
}

# Função para escrever a resposta da requisição HTTP em um arquivo local
def write_response_func(task_instance):
    try:
        response = task_instance.xcom_pull(task_ids="extract_data")
        local_path = "/tmp/extract_data.csv"
        with open(local_path, 'w') as file:
            file.write(response)
        return local_path
    except Exception as e:
        logging.error(f"Erro ao escrever a resposta: {e}")
        raise

# Função para fazer upload do arquivo local para o Google Cloud Storage
def upload_to_gcs_func(**kwargs):
    try:
        task_instance = kwargs['ti']
        local_file_path = task_instance.xcom_pull(task_ids='write_response')
        gcs_client = storage.Client()
        bucket = gcs_client.bucket(BUCKET_NAME)
        blob = bucket.blob(f'data/{FILE_NAME}')
        blob.upload_from_filename(local_file_path)
    except Exception as e:
        logging.error(f"Erro ao fazer upload para o GCS: {e}")
        raise
# Configurações do DAG
default_args = {
    "owner": "grazia.lima",
    "depends_on_past": False,
    "email": ["grazia.lima@avenuecode"],
    "email_on_failure": False
}

# Definindo o DAG
with DAG(
        dag_id="dag_combustivel",
        default_args=default_args,
        description="Dag de carga de dados dos combustíveis",
        start_date=datetime(1994,1,1),
        schedule_interval="@once",
        tags=["combustivel"], max_active_runs=3
) as dag:
    # Inicia o DAG
    start_dag = DummyOperator(task_id="start_dag")

    # Finaliza o DAG
    fim_dag = DummyOperator(task_id="fim_dag")
    
    # Cria o cluster Dataproc
    create_cluster = DataprocCreateClusterOperator(
    task_id="create_cluster",
    gcp_conn_id='google_cloud_default',
    project_id=PROJECT_ID,
    cluster_config=CLUSTER_CONFIG,
    region=REGION,
    cluster_name=CLUSTER_NAME,
    )

    # Tarefa para extrair dados via requisição HTTP
    extract_data = SimpleHttpOperator(
        task_id='extract_data',
        method='GET',
        http_conn_id='',  # Este deve ser configurado no Airflow com a URL base
        endpoint=URL,
        response_filter=lambda response: response.text,
        log_response=True,
    )

    # Processa e salva a resposta da requisição HTTP
    write_response = PythonOperator(
        task_id="write_response",
        python_callable=write_response_func,
    )

    # Faz upload do arquivo para o Google Cloud Storage
    upload_to_gcs = PythonOperator(
        task_id='upload_to_gcs',
        python_callable=upload_to_gcs_func,
        provide_context=True,
    )

    # Submete o job PySpark no cluster Dataproc
    submit_job = DataprocSubmitJobOperator(
        task_id="submit_job", job=PYSPARK_JOB, region=REGION, project_id=PROJECT_ID
    )

    # Deleta o cluster Dataproc após a execução do job
    delete_cluster = DataprocDeleteClusterOperator(
        task_id="delete_cluster",
        project_id=PROJECT_ID,
        cluster_name=CLUSTER_NAME,
        region=REGION,
    )


    start_dag >> create_cluster >> extract_data >> write_response >> upload_to_gcs >> submit_job >> delete_cluster >> fim_dag  
  