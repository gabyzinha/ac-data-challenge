import sys
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, when, substring, input_file_name, regexp_replace, when, trim, to_date
from pyspark.sql.types import DoubleType,StringType,LongType

# Define os parâmetros e lê os argumentos de linha de comando
path_input = sys.argv[1]
path_output = sys.argv[2]
formato = sys.argv[3]
table_bq = sys.argv[4]

project = 'ac-data-challenge'
bucket = 'dc-solution-bucket'

spark = (SparkSession.builder \
            .appName("Processamento de Dados de Gasolina no Brasil") \
            .config("spark.jars.packages", 'com.google.cloud.spark:spark-bigquery-with-dependencies_2.12:0.39.0')
            .getOrCreate()
            )

spark.conf.set("google.cloud.auth.service.account.enable", "true")
spark.conf.set("google.cloud.auth.service.account.json.keyfile", "./credentials.json")

def rename_columns(df):
    renamed_columns = []
    columns = df.columns
    for column in columns:
        renamed_column = column.lower().replace(' - ', '_').replace(' ', '_').replace('-', '_').replace('.', '_').replace('#', 'num').replace('@', 'at').replace('$', 's').replace('%', 'percent').replace('/', '_').replace('[', '_').replace(']', '_').replace('(', '_').replace(')', '_').replace('{', '_').replace('}', '_')[:300]
        renamed_columns.append(renamed_column)
    
    renamed_dataframe = df
    for old_column, new_column in zip(columns, renamed_columns):
        renamed_dataframe = renamed_dataframe.withColumnRenamed(old_column, new_column)
    return renamed_dataframe

def add_semestre(dataframe,coluna):
    dataframe_with_semestre = dataframe.withColumn("semestre", when(substring(col(coluna), 4, 2).cast(LongType()) <= 6, 1).otherwise(2))
    return dataframe_with_semestre

def add_year(dataframe, coluna: str):
    """
    Parametros: dataframe, coluna

    Escreva uma função que receba um Dataframe Spark e o nome da coluna que será baseada para criar 
    uma coluna `year` no Dataframe com os dados lidos do GCS. 
    O resultado da coluna deverá ser o Ano.
    E retorne o dataframe com os dados e a nova coluna criada.
    """
    dataframe_with_year = dataframe.withColumn("year", substring(col(coluna), 7, 4).cast(LongType()))
    return dataframe_with_year

def add_filename_input(df):
    """
    Parametros: dataframe

    Escreva uma função que receba um Dataframe Spark que crie uma coluna `input_file_name` que será baseada no nome do arquivo lido.
    E retorne o dataframe com os dados e a nova coluna criada.
    """
    df_with_filename = df.withColumn('input_file_name', input_file_name().cast(StringType()))
    return df_with_filename

def put_file_gcs(path_output: str, df, formato: str):
    """
    :param path_output: path para save dos dados
    :param dataframe: conjunto de dados a serem salvos
    :param formato: tipo de arquivo a ser salvo
    :return: None

    Escreva uma função que salve os dados no GCS, utilizando o metodo write do Dataframe Spark.
    Tip: 
    """
    try:
        df.coalesce(1).write.format(formato).save(path_output, mode='append')
        return print("Data saved to GCS successfully!")
    except Exception as e:
        return print("Error:", e)

def write_bigquery(dataframe, tabela: str, temporaryGcsBucket):
    """
    Crie uma função que receba os parametros:
    :param dataframe: conjunto de dados a serem salvos
    :param tabela: Tabela do BigQuery que será salvo os dados. Ex: dataset.tabela_exemplo
    :param temporaryGcsBucket: Bucket temporário para salvar area de staging do BigQuery.
    
    E escreva dentro do BigQuery.
    Utilize o material de referencia:
    https://cloud.google.com/dataproc/docs/tutorials/bigquery-connector-spark-example#running_the_code
    """
    try:       
        dataframe.write.format("bigquery").option("temporaryGcsBucket",temporaryGcsBucket).mode("append").save(tabela)
        return print("Data writed to BigQuery successfully!")
    except Exception as e:
        return print("Error:", e)

if __name__ == '__main__':
    try:
        """
        Crie uma função main que receba como parametro:
        path_input: Caminho dos dados no GCS gerados pela API coletora. Ex: gs://bucket_name/file_name
        path_output: Caminho de onde será salvo os dados processados. Ex: gs://bucket_name_2/file_name
        formato_file_save: Formato de arquivo a ser salvo no path_output. Ex: PARQUET
        tabela_bq: Tabela do BigQuery que será salvo os dados. Ex: dataset.tabela_exemplo
        """     
        # Aqui você usará os valores dos parâmetros lidos do sys.argv
        
        # 1 - Faça a leitura dos dados de acordo com o path_input informado
        df = spark.read.csv(path_input, sep=';',inferSchema=True, header=True)

        # 2 - Realize o rename de colunas do arquivo, respeitando os padroes do BigQuery
        df = rename_columns(df)

        # 3 - Adicione uma coluna de Ano, baseado na coluna `Data da Coleta`
        df = add_year(df, 'data_da_coleta')

        # 4 - Adicione uma coluna de Semestre, baseado na coluna de `Data da Coleta`
        df = add_semestre(df, 'data_da_coleta')

        # 5 - Adicione uma coluna Filename. Tip: pyspark.sql.functions.input_file_name
        df = add_filename_input(df)

        # 6 - Faça o parse dos dados lidos de acordo com a tabela no BigQuery
        df_bq = spark.read.format("bigquery").option("table", f"{project}.{table_bq}").load()
        df_bq.show()
        df = df.withColumn("valor_de_venda", trim(regexp_replace("valor_de_venda", ",", ".")))
        df = df.withColumn("valor_de_venda",df["valor_de_venda"].cast(DoubleType()))

        df = df.withColumn("valor_de_compra", trim(regexp_replace("valor_de_compra", ",", ".")))
        df = df.withColumn("valor_de_compra",df["valor_de_compra"].cast(DoubleType()))

        df = df.withColumn("data_da_coleta", trim(col("data_da_coleta")))
        df = df.withColumn("data_da_coleta", regexp_replace("data_da_coleta", "/", "-"))
        df = df.withColumn("data_da_coleta", to_date(col("data_da_coleta"), "dd-MM-yyyy"))

        # 7 - Escreva os dados no Bucket GCS, no caminho informado `path_output` 
        #   no formato especificado no atributo `formato_file_save`.
        put_file_gcs(path_output, df, formato)

        # 8 - Escreva os dados no BigQuery de acordo com a tabela especificada no atributo `tabela_bq`
        write_bigquery(df, table_bq, 'dc-bq-staging')
    
    except Exception as ex:
        print(ex)
